#!/usr/bin/env bash

# Source needed files
source haste
source telegram

# Python setup
sudo ln -sf /usr/bin/pip3.10 /usr/bin/pip3
sudo ln -sf /usr/bin/pip3.10 /usr/bin/pip
sudo ln -sf /usr/bin/python3.10 /usr/bin/python3
sudo ln -sf /usr/bin/python3.10 /usr/bin/python

# System Information
nproc
nproc --all
neofetch
df -h
speedtest

# setup
git config --global credential.helper store
echo -e "https://Sushrut1101:${GH_TOKEN}@github.com" >> ~/.git-credentials

# sync
SRC_DIR=~/lineage

mkdir -p $SRC_DIR
cd $SRC_DIR

repo init --depth=1 -u https://github.com/LineageOS/android.git -b lineage-18.1
repo sync --force-sync -j$(nproc) --no-tags --no-clone-bundle

# Clone deps
git clone --depth=1 --single-branch https://github.com/Redmi-MT6765/device_xiaomi_dandelion.git device/xiaomi/dandelion
git clone --depth=1 --single-branch https://github.com/Redmi-MT6765/vendor_xiaomi_dandelion.git vendor/xiaomi/dandelion
git clone --depth=1 --single-branch https://github.com/Redmi-MT6765/kernel_xiaomi_dandelion.git kernel/xiaomi/dandelion

# Lunch and Build
source bui*/env*.sh

lunch lineage_dandelion-eng
mka -j$(nproc) bacon |& haste

# Upload it

export LINK=$(transfer wet --silent out/target/product/dandelion/lineage-18.1*dandelion.zip)
echo -e $LINK
telegram_message $LINK
